
#include <Ethernet.h>


// Network
const int port = 8768;
IPAddress ip(192, 168, 1, 7);
byte mac[] = {0xDE, 0xAD, 0xBE, 0xEF, 0xFE, 0xED};
EthernetServer server(port);


// Outputs
const int LEFT_FORWARDS_THRUSTER = 2;
const int RIGHT_FORWARDS_THRUSTER = 3;
const int FRONT_DOWNWARDS_THRUSTER = 4;
const int BACK_DOWNWARDS_THRUSTER = 5;


// Grouping outputs
const int THRUSTER[] = {
    LEFT_FORWARDS_THRUSTER,
    RIGHT_FORWARDS_THRUSTER,
    FRONT_DOWNWARDS_THRUSTER,
    BACK_DOWNWARDS_THRUSTER
};

const int THRUSTERS =
    sizeof(THRUSTER) / sizeof(int);


bool bypass_tests = false; // Tests outputs in sequence
void test(const int dig_out[], int outputs, int interval) {
    Serial.print("Testing outputs:\n");
    if (bypass_tests) {
        Serial.println("(bypassed)");
        return;
    }
    for (int i = 0; i < outputs; i++) {
        delay(interval);
        Serial.print(
            String(dig_out[i]) +
            String(", ")
        );
        digitalWrite(dig_out[i], HIGH);
        delay(interval);
        digitalWrite(dig_out[i], LOW);
    }

    Serial.print("\n");
}


void requestString(EthernetClient eth_client, char * string) {
    int i = 0;
    while (eth_client.connected()) {
        if (eth_client.available()) {
            byte c = eth_client.read();
            if (c == -1) break;
            string[i] = c;
            i++;
        }
        else {
            Serial.println("done reading");
            string[i] = '\0';
            break;
        }
    }
}


void applyThrust(char * string) {
    char * body;
    char * pair;
    char * var;
    char * value;

    int i = 0;
    body = strcpy(body, string);
    body = strtok(body, "\r\n\r\n");
    body = strtok(NULL, "\r\n\r\n");

    while ((pair = strtok_r(body, "&", &body))) {
        Serial.print("decoding var ");
        var = strtok(pair, "=");
        value = strtok(NULL, "=");
        digitalWrite(THRUSTER[i], atoi(value)); i++;
        Serial.println(
                String(var)
            + String(" = ")
            + String(value)
        );
    }
}


void setup() {
    Serial.begin(9600);
    Ethernet.begin(mac, ip);
    server.begin();

    Serial.println("\nUROV setup");
    Serial.println(Ethernet.localIP());

    Serial.println("Enabling all outputs");
    for ( // all outputs
        int i = 0;
        i < THRUSTERS;
        i++
    ) { // set as output
        pinMode(THRUSTER[i], OUTPUT);
        Serial.print(
            String(THRUSTER[i]) +
            String(", ")
        );
    }

    test(THRUSTER, THRUSTERS, 1000);
    Serial.println("\nUROV loop:");
}


bool status_printed = false;
void loop() {
    EthernetClient _client = server.available();

    if (!status_printed) {
        Serial.println("Listening on port " + String(port));
        status_printed = true;
    }

    if (_client) {
        char * req;
        Serial.println("reading client");
        requestString(_client, req);
        const unsigned int len = sizeof(req)/sizeof(char);
        Serial.println(len);
        Serial.print(req);
        applyThrust(req);
        status_printed = false;
    }
}
